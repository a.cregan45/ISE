# ImageSearchEngine

find Java classes in "src".

images used stored in src/gallery and src/test.

report material and tests stored in "documents" 


Application Notes:
*If using Eclipse*

1) Open the Eclipse project.
2) Right-click on the main class file for your project.
3) Select "Run As" and then "Run Configurations..." from the context menu.
4) In the "Run Configurations" dialog box that appears, select the "Arguments" tab.
5) In the "VM arguments" field, enter "-Xmx9216m", to increase the maximum heap size.
6) Click "Apply" to save the changes.